Rails.application.routes.draw do
  devise_for :users
  get 'initialpage/index'
  get 'welcome/index'

  root 'initialpage#index'

  resources :users
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
